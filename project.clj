(defproject offcourse/ui-components "0.1.19-SNAPSHOT"
  :description "This repo contains the code for the ui components for the offcourse platform"
  :url "http://gitlab.com/offcourse/ui-components"
  :license         {:name "MIT License"
                    :url "http://opensource.org/licenses/MIT"}
  :min-lein-version "2.7.1"
  :plugins [[lein-figwheel "0.5.13"]
            [lein-pprint "1.1.2"]
            [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]]
  :dependencies [[offcourse/bootlaces           "0.1.16"         :scope "test"]
                 [org.clojure/clojurescript     "1.9.854"]
                 [reagent                       "0.8.0-alpha1"]
                 [markdown-clj                  "0.9.99"]
                 [offcourse/shared              "0.11.5"]
                 [funcool/cuerdas               "2.0.3"]
                 [devcards                      "0.2.4"]
                 [cljsjs/react                  "15.6.2-0"]
                 [cljsjs/react-dom              "15.6.2-0"]
                 [cljsjs/clipboard              "1.6.1-1"]
                 [cljsjs/react-xmasonry         "2.0.1-1"]
                 [cljsjs/react-sticky           "5.0.7-0"]
                 [cljsjs/semantic-ui-react      "0.71.0-0"]]
  :source-paths ["src"]
  :repositories [["snapshots" {:url "https://clojars.org/repo"
                               :username :env/CLOJARS_USER
                               :password :env/CLOJARS_PASS}]
                  ["releases" {:url "https://clojars.org/repo"
                              :sign-releases false
                              :username :env/CLOJARS_USER
                              :password :env/CLOJARS_PASS}]]
  :deploy-repositories [["clojars" {:url "https://clojars.org/repo"
                                    :username :env/CLOJARS_USER
                                    :password :env/CLOJARS_PASS
                                    :sign-releases false}]]
  :aliases {"bump" ["change" "version" "leiningen.release/bump-version"]}
  :release-tasks [["change" "version" "leiningen.release/bump-version" "release"]
                  ["deploy" "clojars"]]
  :cljsbuild {:builds
              [{:id :devcards
                :source-paths ["src"]
                :figwheel { :devcards true }
                :compiler {:main "ui-components.cards.core"
                           :asset-path "js/out"
                           :output-to "resources/public/js/ui-components.js"
                           :output-dir "resources/public/js/out"
                           :source-map-timestamp true
                           :preloads [devtools.preload]}}]}
  :figwheel {:css-dirs ["resources/public/css"]
             :server-port 7777
             :open-file-command "emacsclient"}
  :profiles {:dev {:dependencies [[binaryage/devtools "0.9.4"]
                                  [figwheel-sidecar "0.5.13"]
                                  [com.cemerick/piggieback "0.2.2"]]
                   :source-paths ["src"]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                   :clean-targets ^{:protect false} ["resources/public/js/compiled"
                                                     :target-path]}})
