(ns ui-components.button
  (:require [ui-components.forms.utils :refer [prevent-default]]
            [ui-components.semantic :as s]
            [shared.protocols.loggable :as log]))

(defn Button [{:keys [primary on-click color loading? disabled?]} text]
  [s/Button {:primary primary
             :onClick #(prevent-default % on-click)
             :color color
             :disabled disabled?
             :loading loading?} text])
