(ns ui-components.masonry
  (:require [reagent.core :as r]
            [ui-components.semantic :refer [Item Loader]]
            [cljsjs.react-xmasonry]))

(def XMasonry (r/adapt-react-class js/XMasonry))
(def XBlock (r/adapt-react-class js/XBlock))

(defn wrapper [component]
  (let [key (:key (meta component))]
    (r/as-element [XBlock {:key key} (r/as-element component)])))

(defn Masonry [components]
  (let [elements (map #(wrapper %) components)]
    [XMasonry {:id "grid"
               :center true
               :responsive true
               :targetBlockWidth 300}
     (clj->js elements)]))
