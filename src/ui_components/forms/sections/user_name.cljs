(ns ui-components.forms.sections.user-name
  (:require [ui-components.semantic :refer [FormField FormInput]]
            [ui-components.forms.sections.message :refer [Message]]
            [shared.protocols.loggable :as log]))

(defn UserNameSection [{:keys [user-name errors update]}]
  [FormField {:error (boolean errors)}
   [:label "User Name"]
   [FormInput {:placeholder "please enter your name..."
               :autoFocus   true
               :value       (or user-name "")
               :onChange    #(update (.-value %2))}]
   [:div
    (keep (fn [error]
            ^{:key error} [Message error]) (:reasons errors))]])
