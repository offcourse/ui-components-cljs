(ns ui-components.forms.sections.goal
  (:require [ui-components.semantic :refer [FormField FormInput]]
            [ui-components.forms.sections.message :refer [Message]]
            [shared.protocols.loggable :as log]))

(defn GoalSection [{:keys [goal errors invalid? editable? errors update]}]
  [FormField (when editable? {:error (boolean errors)})
   [:label "Goal of the Course"]
   [FormInput {:placeholder "Goal"
               :autoFocus   (when editable? true)
               :disabled    (not editable?)
               :value       (or goal "")
               :onChange    #(update (.-value %2))}]
   [:div
    (when editable? (keep (fn [error]
                            ^{:key error} [Message error]) (:reasons errors)))]])
