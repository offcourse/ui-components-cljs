(ns ui-components.forms.sections.description
  (:require [ui-components.semantic :refer [FormField FormTextArea]]
            [ui-components.forms.sections.message :refer [Message]]
            [shared.protocols.loggable :as log]))

(defn DescriptionSection [{:keys [description errors invalid? editable? errors update]}]
  [FormField {:error (boolean errors)}
   [:label "Description of the Course"]
   [FormTextArea {:placeholder "Description"
                  :autoFocus   false
                  :value       (or description "")
                  :onChange    #(update (.-value %2))}]
   [:div
    (when editable? (keep (fn [error]
                            ^{:key error} [Message error]) (:reasons errors)))]])
