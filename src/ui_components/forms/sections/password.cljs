(ns ui-components.forms.sections.password
  (:require [ui-components.semantic :refer [FormField FormInput]]
            [ui-components.forms.sections.message :refer [Message]]
            [shared.protocols.loggable :as log]))

(defn PasswordSection [{:keys [password errors update]}]
  [FormField {:error (boolean errors)}
   [:label "Password"]
   [FormInput {:placeholder "please enter your password..."
               :input       :password
               :value       (or password "")
               :onChange    #(update (.-value %2))}]
   [:div
    (keep (fn [error]
            ^{:key error} [Message error]) (:reasons errors))]])
