(ns ui-components.forms.sections.confirmation-code
(:require [ui-components.semantic :refer [FormField FormInput]]
          [ui-components.forms.sections.message :refer [Message]]
          [shared.protocols.loggable :as log]))

(defn ConfirmationCodeSection[{:keys [confirmation-code errors update]}]
  (when (boolean errors)
    [FormField {:disabled    (not (boolean errors))}
     [:label "Confirmation Code"]
     [FormInput {:placeholder "please enter your cofirmation code..."
                 :autoFocus   (not (boolean errors))
                 :value       (or confirmation-code "")
                 :onChange    #(update (.-value %2))}]
    [:div
     (keep (fn [error]
             ^{:key error} [Message error]) (:reasons errors))]]))
