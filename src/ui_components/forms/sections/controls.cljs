(ns ui-components.forms.sections.controls
  (:require [ui-components.forms.utils :refer [prevent-default]]
            [ui-components.semantic :as s]
            [ui-components.button :refer [Button]]))

(defn FormControls [{:keys [on-cancel on-confirm loading?
                            confirm-message cancel-message]} custom-button]
  [:div
   (when-not loading?
     custom-button
     [Button {:color :red
              :on-click on-cancel} (or cancel-message "Cancel")])
   [Button {:primary true
            :loading? loading?
            :disabled? loading?
            :on-click on-confirm} (or confirm-message "Save")]])
