(ns ui-components.forms.sections.message
  (:require [ui-components.notifications :refer [notifications]]
            [ui-components.semantic :as s]
            [shared.protocols.loggable :as log]))

(defn Message [reason]
  (let [{:keys [type message] :as error} (reason notifications)]
    [s/Message {type true} message]))
