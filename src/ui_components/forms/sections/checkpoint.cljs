(ns ui-components.forms.sections.checkpoint
  (:require [ui-components.semantic :refer [Button FormField FormGroup FormInput]]
            [shared.protocols.loggable :as log]))

(defn CheckpointSection [{:keys [on-change errors on-remove]} {:keys [task resource-url valid?] :as checkpoint}]
  [FormGroup
   [FormField {:width 6
               :error (contains? errors :task)}
    [:label "Task"]
    [FormInput {:placeholder "Task"
                :value (or task "")
                :onChange #(on-change (assoc checkpoint :task (.-value %2)))}]]
   [FormField {:width 9
               :error (contains? errors :resource-url)}
    [:label "Resource Url"]
    [FormInput {:placeholder "Url"
                :value (or resource-url "")
                :onChange #(on-change (assoc checkpoint :resource-url (.-value %2)))}]]
   [FormField {:width 1}
    [:label {:className "invisible"} "."]
    [Button {:fluid true
             :icon "remove"
             :onClick #(on-remove checkpoint)}]]])
