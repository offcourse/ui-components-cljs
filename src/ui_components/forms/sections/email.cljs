(ns ui-components.forms.sections.email
  (:require [ui-components.semantic :refer [FormField FormInput]]
            [ui-components.forms.sections.message :refer [Message]]
            [shared.protocols.loggable :as log]))

(defn EmailSection [{:keys [email errors update]}]
  [FormField {:error (boolean errors)}
   [:label "Email"]
   [FormInput {:placeholder "please enter your email..."
               :value       (or email "")
               :onChange    #(update (.-value %2))}]
   [:div
    (keep (fn [error]
            ^{:key error} [Message error]) (:reasons errors))]])
