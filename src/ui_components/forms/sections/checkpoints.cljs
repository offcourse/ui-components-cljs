(ns ui-components.forms.sections.checkpoints
  (:require [ui-components.forms.sections.checkpoint :refer [CheckpointSection]]
            [ui-components.forms.sections.message :refer [Message]]
            [ui-components.forms.utils :refer [prevent-default debounce submit-form]]
            [ui-components.semantic :refer [Divider]]
            [shared.protocols.loggable :as log]))

(defn CheckpointsSection [{:keys [checkpoints errors remove-checkpoint update-checkpoint]}]
  [:div
   [Divider {:horizontal true} "Checkpoints"]
   (keep (fn [error]
           ^{:key error} [Message error]) (:reasons errors))
   (map-indexed (fn [index checkpoint]
                  ^{:key index} [CheckpointSection
                                 {:on-change update-checkpoint
                                  :errors    (get (:locations errors) index)
                                  :on-remove remove-checkpoint} (assoc checkpoint :index index)])
                checkpoints)])
