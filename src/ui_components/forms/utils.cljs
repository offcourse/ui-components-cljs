(ns ui-components.forms.utils
  (:import goog.async.Debouncer))

(defn debounce [f interval]
  (let [dbnc (Debouncer. f interval)]
    (fn [& args] (.apply (.-fire dbnc) dbnc (to-array args)))))

(defn submit-form [event item handler]
  (.preventDefault event)
  (handler item))

(defn prevent-default [event handler]
  (.preventDefault event)
  (handler))
