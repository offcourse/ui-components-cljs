(ns ui-components.forms.sign-up
  (:require [ui-components.semantic :refer [Button Message Header Segment Form FormField FormInput]]
            [ui-components.forms.sections.user-name :refer [UserNameSection]]
            [ui-components.forms.sections.confirmation-code :refer [ConfirmationCodeSection]]
            [ui-components.forms.sections.password :refer [PasswordSection]]
            [ui-components.forms.sections.email :refer [EmailSection]]
            [ui-components.forms.sections.controls :refer [FormControls]]
            [ui-components.forms.utils :refer [prevent-default]]
            [ui-components.overlay :refer [Overlay]]
            [shared.protocols.specced :as sp]
            [reagent.core :as r]
            [shared.protocols.loggable :as log]))

(defn update-user-name [profile-atom user-name]
  (swap! profile-atom (fn [profile] (assoc profile :user-name user-name))))

(defn update-password [profile-atom password]
  (swap! profile-atom (fn [profile] (assoc profile :password password))))

(defn update-email [profile-atom email]
  (swap! profile-atom (fn [profile] (assoc profile :email email))))

(defn update-confirmation-code [profile-atom confirmation-code]
  (swap! profile-atom (fn [profile] (assoc profile :confirmation-code confirmation-code))))

(defn SignUpForm [{:keys [draft on-submit on-change on-cancel]}]
  (let [profile (r/atom draft)]
    (fn [{:keys [flags errors]}]
      [Overlay {:title "Sign Up"}
       [Form {:error (boolean errors)
              :onSubmit #(prevent-default %1 identity)}
        [UserNameSection {:user-name (:user-name @profile)
                          :errors (-> errors :user-name)
                          :update #(-> (update-user-name profile %))}]
        [EmailSection     {:email (:email @profile)
                           :errors (-> errors :email)
                           :update #(-> (update-email profile %))}]
        [PasswordSection {:password (:password @profile)
                          :errors (-> errors :password)
                          :update #(-> (update-password profile %))}]
        [ConfirmationCodeSection {:confirmation-code (:confirmation-code @profile)
                                  :errors (-> errors :confirmation-code)
                                  :update #(-> (update-confirmation-code profile %))}]]
       [FormControls {:on-cancel on-cancel
                      :on-confirm #(on-submit @profile)
                      :loading?  (:checking? flags)
                      :disabled? (:checking? flags)}]])))
