(ns ui-components.forms.course
  (:require [ui-components.button :refer [Button]]
            [ui-components.forms.sections.checkpoints :refer [CheckpointsSection]]
            [ui-components.forms.sections.controls :refer [FormControls]]
            [ui-components.forms.sections.goal :refer [GoalSection]]
            [ui-components.forms.sections.description :refer [DescriptionSection]]
            [ui-components.forms.utils :refer [prevent-default]]
            [shared.protocols.actionable :as ac]
            [ui-components.overlay :refer [Overlay]]
            [ui-components.semantic :refer [Form]]
            [shared.protocols.loggable :as log]
            [reagent.core :as r]))

(defn update-goal [course-atom goal]
  (swap! course-atom (fn [course] (assoc course :goal goal))))

(defn update-description [course-atom description]
  (swap! course-atom (fn [course] (assoc course :description description))))

(defn update-checkpoint [course-atom checkpoint]
  (swap! course-atom (fn [course] (ac/perform course [:update checkpoint]))))

(defn remove-checkpoint [course-atom checkpoint]
  (swap! course-atom (fn [course] (ac/perform course [:remove checkpoint]))))

(defn create-checkpoint [course-atom]
  (swap! course-atom (fn [course] (ac/perform course [:add :new-checkpoint]))))

(defn CourseForm [{:keys [draft flags on-cancel on-change on-submit]}]
  (let [course (r/atom draft)]
    (fn [{:keys [flags errors]}]
      [Overlay {:title "Edit Course"}
       [Form {:error (boolean errors)
              :onSubmit #(prevent-default %1 identity)}
        [GoalSection {:goal   (:goal @course)
                      :editable? (:goal-editable? flags)
                      :errors (-> errors :goal)
                      :update #(update-goal course %)}]
        [DescriptionSection {:description   (:description @course)
                             :errors (-> errors :description)
                             :update #(update-description course %)}]
        [CheckpointsSection {:update-checkpoint #(update-checkpoint course %1)
                             :remove-checkpoint #(remove-checkpoint course %1)
                             :errors            (-> errors :checkpoints)
                             :checkpoints       (:checkpoints @course)}]]
       [FormControls {:on-cancel  on-cancel
                      :on-confirm #(on-submit @course)
                      :loading?   (:checking? flags)
                      :disabled?  (:checking? flags)}
        [Button {:on-click #(create-checkpoint course)} "Add Checkpoint"]]])))
