(ns ui-components.course-meta
  (:require [ui-components.semantic
             :refer
             [ItemGroup Item ItemDescription ItemContent Image ItemHeader Image ItemMeta]]
            [cuerdas.core :as str]
            [reagent.core :as r]
            [shared.protocols.loggable :as log]))

(defn CourseMeta []
  (let [errors (r/atom {:missing-portrait? false})]
    (fn [{:keys [curator urls] :as course}]
      (let [{:keys [missing-portrait?]} @errors]
      [ItemGroup
       [Item
        [Image {:as :div
                :size :mini
                :src (if-not missing-portrait?
                       (:portrait urls)
                       "/images/avatar.png")
                :onError #(swap! errors update :missing-portrait? not)}]
        [ItemContent {:verticalAlign :top}
         [ItemMeta "Curated by"]
         [ItemHeader [:a {:href (str "/curators/" curator)}(str/capital curator)]]
         #_[ItemDescription "Learners 345 Stats 123 Stats 123"]]]]))))
