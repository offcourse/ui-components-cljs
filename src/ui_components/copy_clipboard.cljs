(ns ui-components.copy-clipboard
  (:require cljsjs.clipboard
            [ui-components.semantic :refer [Button Icon]]
            [reagent.core :as r]))

(defn CopyClipboard [{:keys [url floated]}]
  (let [clipboard-atom (atom nil)]
    (r/create-class
     {:display-name "clipboard-button"
      :component-did-mount
      #(let [clipboard (new js/Clipboard (r/dom-node %))]
         (reset! clipboard-atom clipboard))
      :component-will-unmount
      #(when-not (nil? @clipboard-atom)
         (.destroy @clipboard-atom)
         (reset! clipboard-atom nil))
      :reagent-render
      (fn []
        [Button  {:name :clipboard
                  :basic true
                  :floated floated
                  :data-clipboard-text url} "Get URL"])})))
