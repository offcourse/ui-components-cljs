(ns ui-components.content-meta
  (:require [ui-components.semantic :refer [Segment Header Button]]
            [ui-components.labels :refer [Labels]]))

(defn ContentMeta [{:keys [tags resource-url task]}]
  [Segment {:basic true}
   [:a {:target :_blank
        :href resource-url}
    [Button {:content "Open Original"
             :fluid true
             :icon :external
             :labelPosition :right}]]
   [Header {:sub true} "Task:"]
   [:span task]
   [Header {:sub true} "Tags"]
   (when-not (empty? tags) [Labels tags])])
