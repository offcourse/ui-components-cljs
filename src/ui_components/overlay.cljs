(ns ui-components.overlay
  (:require [shared.protocols.loggable :as log]
            [ui-components.forms.sections.controls :refer [FormControls]]
            [ui-components.semantic :refer [Form Modal ModalHeader ModalContent ModalActions Segment]]))

(defn Overlay [{:keys [title basic size]} content actions]
  [Modal {:open true
          :basic (or basic false)
          :size (or size :large)}
   [ModalHeader title]
   [ModalContent content]
   [ModalActions actions]])
