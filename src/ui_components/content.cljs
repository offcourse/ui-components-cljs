(ns ui-components.content
  (:require [clojure.string :as c-str]
            [ui-components.semantic :refer [Embed]]
            [markdown.core :refer [md->html]]
            [shared.protocols.loggable :as log]
            [shared.protocols.specced :as sp]))

(defn process-content [content]
  (-> content
      (c-str/replace "<img " "<img class='ui fluid image'")
      md->html))

(defn error-message [description]
  (if description
    [:div [:strong "Description: "] description]
    [:div [:strong] "No description available"]))

(defmulti Content #(:resource-type %1))
(defmethod Content "video" [{:keys [title description content] :as resource}]
  [:div
   [:h1.title {:key :title} (or title "--- no title ---")]
   [Embed {:id (:id content)
            :source (name (:provider content))}]
   #_[:article {:key :content
              :dangerouslySetInnerHTML (when content {:__html (process-content content)})}
    (when-not content (error-message description))]])

(defmethod Content "html" [{:keys [title description content] :as resource}]
  [:div
   [:h1.title {:key :title} (or title "--- no title ---")]
   [:article {:key :content
              :dangerouslySetInnerHTML (when content {:__html (process-content content)})}
    (when-not content (error-message description))]])
