(ns ui-components.flash
  (:require [ui-components.semantic :refer [Grid GridRow GridColumn Message]]))

(defn Flash [{:keys [type message]}]
  [Grid {:padded true}
   [GridRow
    [GridColumn {:width 2}]
    [GridColumn {:width 12}
     [Message {type true} message]]]])
