(ns ui-components.notifications)

(def notifications
  {:not-logged-in               {:type :warning
                                 :message "You need to be logged in before you can create courses"}
   :not-found-resource           {:type :warning
                                  :message "Sorry, we couldn't fetch this resource. Please use the original instead..."}
   :not-authenticated            {:type :warning
                                  :message "You need to be authenticated before you can sign up"}
   :invalid-goal                 {:type :error
                                  :message "A goal should consist of at least 4 characters"}
   :existing-goal                {:type :error
                                  :message "You already have a course with this title!"}
   :existing-user-name           {:type :error
                                  :message "This user name has already been taken"}
   :invalid-resource-url         {:type :error
                                  :message "Only valid urls allowed"}
   :invalid-task                 {:type :error
                                  :message "A task should have at least 4 characters"}
   :invalid-user-name            {:type :error
                                  :message "There should be a valid user name"}
   :invalid-email                {:type :error
                                  :message "There should be a valid email address"}
   :invalid-password             {:type :error
                                  :message "There should be a valid password"}
   :unconfirmed-user             {:type :error
                                  :message "We need a confirmation code"}
   :invalid-confirmation-code   {:type :error
                                  :message "We need a valid confirmation code"}
   :invalid-checkpoints          {:type :error
                                  :message "A course should have at least 2 checkpoints"}})
