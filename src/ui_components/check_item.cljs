(ns ui-components.check-item
  (:require [reagent.core :as r]
            [ui-components.semantic :refer [Checkbox ListContent ItemContent ItemImage ListItem Segment]]
            [shared.protocols.loggable :as log]))

(defn CheckItem [{:keys [completed? selected? trackable? url task on-change] :as item}]
  [ListItem {:active selected?}
   (when trackable? [ItemImage [Checkbox {:checked completed?
                                          :onChange #(on-change item)}]])
    [ItemContent [:a {:href url} task]]])
