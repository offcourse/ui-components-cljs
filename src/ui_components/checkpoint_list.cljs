(ns ui-components.checkpoint-list
  (:require [ui-components.check-item :refer [CheckItem]]
            [ui-components.semantic :refer [List]]
            [shared.protocols.loggable :as log]))

(defn CheckpointList [{:keys [checkpoints status course-id on-check trackable?]}]
  [List {:relaxed true
         :selection true}
   (map (fn [{:keys [checkpoint-id flags checkpoint-url] :as checkpoint}]
          (let [item (-> checkpoint
                         (assoc :url checkpoint-url
                                :selected? (:selected? flags)
                                :completed? (:completed? flags)
                                :trackable? trackable?
                                :on-change on-check))]
            ^{:key checkpoint-id} [CheckItem item])) checkpoints)])
