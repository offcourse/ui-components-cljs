(ns ui-components.labels
  (:require [clojure.string :as c-str]
            [cuerdas.core :as str]
            [ui-components.semantic :refer [Label LabelGroup]]))

(defn Labels [items]
  [LabelGroup
   (map (fn [tag] [Label {:as :a
                          :href (str "/tags/" (str/slug tag))
                          :size "mini"
                          :key tag} tag]) items)])
