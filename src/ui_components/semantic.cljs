(ns ui-components.semantic
  (:refer-clojure :exclude [List])
  (:require [reagent.core :as r]
            [cljsjs.semantic-ui-react]))

(def semantic-ui js/semanticUIReact)

(def Container   (r/adapt-react-class (goog.object/get semantic-ui "Container")))

(def Header        (r/adapt-react-class (goog.object/get semantic-ui "Header")))
(def Subheader     (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Header" "Subheader")))
(def HeaderContent (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Header" "Content")))

(def JSItem      (goog.object/get semantic-ui "Item"))
(def Item        (r/adapt-react-class (goog.object/get semantic-ui "Item")))
(def ItemImage   (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Item" "Image")))
(def ItemHeader  (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Item" "Header")))
(def ItemMeta    (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Item" "Meta")))
(def ItemContent (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Item" "Content")))
(def ItemDescription (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Item" "Description")))
(def ItemGroup   (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Item" "Group")))

(def Embed        (r/adapt-react-class (goog.object/get semantic-ui "Embed")))

(def JSButton    (goog.object/get semantic-ui "Button"))
(def Button      (r/adapt-react-class (goog.object/get semantic-ui "Button")))
(def Buttons   (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Button" "Group")))

(def JSSegment    (goog.object/get semantic-ui "Segment"))
(def Segment      (r/adapt-react-class (goog.object/get semantic-ui "Segment")))
(def SegmentGroup (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Segment" "Group")))

(def JSLabel    (goog.object/get semantic-ui "Label"))
(def Label      (r/adapt-react-class (goog.object/get semantic-ui "Label")))
(def LabelGroup (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Label" "Group")))

(def Form       (r/adapt-react-class (goog.object/get semantic-ui "Form")))
(def FormField  (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Form" "Field")))
(def FormGroup  (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Form" "Group")))
(def FormInput  (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Form" "Input")))
(def FormTextArea (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Form" "TextArea")))

(def JSMenu     (goog.object/get semantic-ui "Menu"))
(def Menu      (r/adapt-react-class (goog.object/get semantic-ui "Menu")))
(def MenuItem  (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Menu" "Item")))
(def MenuMenu  (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Menu" "Menu")))

(def Image      (r/adapt-react-class (goog.object/get semantic-ui "Image")))

(def JSIcon     (goog.object/get semantic-ui "Icon"))
(def Icon       (r/adapt-react-class (goog.object/get semantic-ui "Icon")))

(def Loader     (r/adapt-react-class (goog.object/get semantic-ui "Loader")))

(def JSList      (goog.object/get semantic-ui "List"))
(def List        (r/adapt-react-class (goog.object/get semantic-ui "List")))
(def ListItem    (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "List" "Item")))
(def ListContent (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "List" "Content")))


(def Message       (r/adapt-react-class (goog.object/get semantic-ui "Message")))

(def Modal       (r/adapt-react-class (goog.object/get semantic-ui "Modal")))
(def ModalHeader (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Modal" "Header")))
(def ModalContent (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Modal" "Content")))
(def ModalActions (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Modal" "Actions")))

(def Popup       (r/adapt-react-class (goog.object/get semantic-ui "Popup")))

(def Checkbox      (r/adapt-react-class (goog.object/get semantic-ui "Checkbox")))
(def Divider       (r/adapt-react-class (goog.object/get semantic-ui "Divider")))

(def JSCard          (goog.object/get semantic-ui "Card"))
(def Card            (r/adapt-react-class (goog.object/get semantic-ui "Card")))
(def CardContent     (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Card" "Content")))
(def CardHeader      (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Card" "Header")))
(def CardMeta        (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Card" "Meta")))

(def JSSidebar       (goog.object/get semantic-ui "Sidebar"))
(def Sidebar (r/adapt-react-class JSSidebar))
(def SidebarPushable (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Sidebar" "Pushable")))
(def SidebarPusher   (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Sidebar" "Pusher")))

(def JSAccordion      (goog.object/get semantic-ui "Accordion"))
(def Accordion        (r/adapt-react-class (goog.object/get semantic-ui "Accordion")))
(def AccordionTitle   (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Accordion" "Title")))
(def AccordionContent (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Accordion" "Content")))

(def JSTable          (goog.object/get semantic-ui "Table"))
(def Table            (r/adapt-react-class (goog.object/get semantic-ui "Table")))
(def TableHeader      (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Header")))
(def TableBody        (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Body")))
(def TableFooter      (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Footer")))
(def TableRow         (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Row")))
(def TableHeaderCell  (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "HeaderCell")))
(def TableCell        (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Table" "Cell")))

(def JSGrid           (goog.object/get semantic-ui "Grid"))
(def JSGridColumn     (goog.object/getValueByKeys semantic-ui "Grid" "Column"))
(def Grid             (r/adapt-react-class (goog.object/get semantic-ui "Grid")))
(def GridRow          (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Grid" "Row")))
(def GridColumn       (r/adapt-react-class (goog.object/getValueByKeys semantic-ui "Grid" "Column")))
