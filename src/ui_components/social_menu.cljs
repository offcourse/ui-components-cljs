(ns ui-components.social-menu
  (:require [ui-components.copy-clipboard :refer [CopyClipboard]]
            [ui-components.semantic :refer [ItemGroup Buttons Button Popup Icon Menu Item MenuItem]]
            [reagent.core :as r]))

(defn tweet [text url]
  (.open js/window (str "http://twitter.com/share?url=" url "&text=" text) "tshare" "height=260,width=550,resizable=0,toolbar=0,menubar=0,status=0,location=0"))


(defn fb [text url]
  (.open js/window (str "http://facebook.com/sharer.php?u=" url) "fbshare" "height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0"))

(def handlers
  {:twitter tweet
   :facebook fb})

(defn SocialProvider [provider url]
  (let [handler (provider handlers)]
    [Icon {:onClick #(handler "Cool!" url)
           :disabled (not handler)
           :name provider
           :size :large}]))

(defn SocialMenu [{:keys [providers url]}]
  [Item
   (map (fn [provider] ^{:key provider} [SocialProvider provider url]) providers)
   [CopyClipboard {:url url
                   :floated :right}]])



