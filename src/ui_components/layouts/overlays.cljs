(ns ui-components.layouts.overlays
  (:require [ui-components.forms.course :refer [CourseForm]]
            [ui-components.forms.sign-in :refer [SignInForm]]
            [ui-components.confirm :refer [Confirm]]
            [shared.protocols.loggable :as log]))

(defn overlays [{:keys [user course drafts flags handlers]}]
  (let [{:keys [switch-to check-user sign-up fork-course
                go-to-fork save-course]} handlers]
    {:fork-modal     [Confirm     {:course     course
                                   :title      "Want to Fork?"
                                   :content    "Lorem Ipsum Tralalala"
                                   :confirm-message "Fork"
                                   :on-cancel #(switch-to :view-mode)
                                   :on-confirm fork-course}]
     :redirect-modal [Confirm     {:course     course
                                   :title      "Want to Redirect?"
                                   :content    "Lorem Ipsum Tralalala"
                                   :confirm-message "Redirect"
                                   :on-cancel #(switch-to :view-mode)
                                   :on-confirm (partial go-to-fork course)}]
     :sign-in-mode   [SignInForm {:draft     (-> drafts :profile)
                                  :errors    (-> drafts :profile :errors)
                                  :flags     (-> drafts :profile :flags)
                                  :on-cancel (:sign-out handlers)
                                  :on-submit (:sign-in handlers)}]
     :edit-mode      [CourseForm  {:draft     (-> drafts :course)
                                   :flags     (-> drafts :course :flags)
                                   :errors    (-> drafts :course :errors)
                                   :on-cancel #(switch-to :view-mode)
                                   :on-submit (:save-draft handlers)}]}))
