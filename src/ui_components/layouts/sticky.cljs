(ns ui-components.layouts.sticky
  (:require [reagent.core :as r]
            [cljsjs.react-sticky]))

(def -StickyContainer (r/adapt-react-class (goog.object/get js/ReactSticky "StickyContainer")))
(def -Sticky (r/adapt-react-class (goog.object/get js/ReactSticky "Sticky")))

(defn StickyContainer [menubar main]
  [-StickyContainer {:id :wrapper}
   [-Sticky {:style {:zIndex 5}} menubar]
   main])
