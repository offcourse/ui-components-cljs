(ns ui-components.layouts.sign-up
  (:require [ui-components.layouts.base :refer [BaseLayout]]
            [ui-components.forms.sign-up :refer [SignUpForm]]
            [shared.protocols.loggable :as log]
            [ui-components.semantic :refer [Modal]]))

(defn SignUpLayout [{:keys [profile-draft flags handlers] :as view-data}]
  [BaseLayout view-data
   [SignUpForm {:draft     profile-draft
                :errors    (:errors profile-draft)
                :flags     (:flags profile-draft)
                :on-cancel (:sign-out handlers)
                :on-submit (:sign-up handlers)}]])
