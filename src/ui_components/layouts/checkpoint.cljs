(ns ui-components.layouts.checkpoint
  (:require [ui-components.semantic :refer [Segment Grid GridColumn GridRow]]
            [ui-components.course-card :refer [CourseCard]]
            [ui-components.control-panel :refer [ControlPanel]]
            [ui-components.dashboard :refer [Dashboard]]
            [ui-components.layouts.base :refer [BaseLayout]]
            [ui-components.viewer :refer [Viewer]]
            [shared.protocols.loggable :as log]))

(defn bind-handlers [handlers flags course]
  {:edit-course (partial (:edit-course handlers) course)
   :go-to-fork  #((:switch-to handlers) :redirect-modal)
   :toggle-status (cond
                    (:signed-in? flags) (:toggle-status handlers)
                    (:signed-out? flags) (:sign-in handlers)
                    :default identity)
   :fork-course #((:switch-to handlers) :fork-modal)
   :sign-in (partial (:sign-in handlers) nil)})

(defn Main [{:keys [handlers flags course resource]}]
  (let [handlers     (bind-handlers handlers flags course)]
    [GridColumn
     (when-not (:dashboard-hidden? flags)
       [Dashboard
        [CourseCard {:course        course
                     :trackable?    (:signed-in? flags)
                     :toggle-status (:toggle-status handlers)}]
        [ControlPanel {:flags (:flags course)
                       :handlers handlers}]])
     [Viewer {:resource   resource}]]))

(defn CheckpointLayout [{:keys [flags user course resource checkpoint  handlers] :as view-data}]
  [BaseLayout
   view-data
   [Grid {:padded true}
    [GridRow {:only :mobile}
     [Main {:flags (assoc flags :dashboard-hidden? false)
            :handlers handlers
            :course course
            :resource resource}]]
    [GridRow {:only :tablet}
     [Main {:flags flags
            :handlers handlers
            :course course
            :resource resource}]]
    [GridRow {:only :computer}
     [Main {:flags flags
            :handlers handlers
            :course course
            :resource resource}]]]])
