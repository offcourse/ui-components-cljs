(ns ui-components.layouts.new-course
  (:require [ui-components.forms.course :refer [CourseForm]]
            [ui-components.layouts.base :refer [BaseLayout]]))

(defn NewCourseLayout [{:keys [user flags course-draft handlers] :as view-data}]
  [BaseLayout view-data
   (when (= (:status user) :signed-in)
     [CourseForm {:draft       course-draft
                  :flags       (:flags course-draft)
                  :errors      (:errors course-draft)
                  :checkpoints (:checkpoints course-draft)
                  :on-cancel   (:go-home handlers)
                  :on-submit   (:save-draft handlers)}])])
