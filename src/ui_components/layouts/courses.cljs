(ns ui-components.layouts.courses
  (:require [ui-components.layouts.base :refer [BaseLayout]]
            [ui-components.course-card :refer [CourseCard]]
            [shared.protocols.loggable :as log]
            [ui-components.semantic :refer [Segment Grid GridRow GridColumn Item Loader]]
            [ui-components.masonry :refer [Masonry]]))

(defn CoursesLayout [{:keys [courses handlers] :as view-data}]
  [BaseLayout
   view-data
   [Grid {:padded true}
    [GridColumn
     (if-not (empty? courses)
       [Masonry
        (map (fn [{:keys [course-id] :as course}]
               ^{:key course-id} [CourseCard {:course course
                                              :toggle-checkpoint (:toggle-checkpoint handlers)}])
             courses)]
       [Segment {:basic true}
        [Loader {:active true
                 :size :massive
                 :inline :centered} "Loading..."]])]]])
