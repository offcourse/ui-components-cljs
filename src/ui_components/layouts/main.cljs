(ns ui-components.layouts.main
  (:require [ui-components.flash :refer [Flash]]
            [ui-components.semantic :refer [Grid GridRow GridColumn Button Icon JSItem]]
            [ui-components.notifications :refer [notifications]]
            [shared.protocols.loggable :as log]))

(defn Main [{:keys [notification]} content]
  [:div
   (when notification [Flash (notification notifications)])
   content])
