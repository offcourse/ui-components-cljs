(ns ui-components.layouts.sidebar
  (:require [ui-components.sidebar :refer [Sidebar]]
            [ui-components.semantic :refer [SidebarPushable SidebarPusher]]
            [reagent.core :as r]))

(def ui-state (r/atom false))

(defn toggle-sidebar []
  (swap! ui-state not))

(defn SidebarContainer [sidebar modal main]
  (let [sidebar-visible? (= (:sidebar @ui-state) :visible)]
  [SidebarPushable
   #_[Sidebar {:visible? @ui-state
             :toggle toggle-sidebar}
    sidebar]
   [SidebarPusher
    modal
    main]]))
