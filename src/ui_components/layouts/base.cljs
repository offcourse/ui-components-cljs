(ns ui-components.layouts.base
  (:require [reagent.core :as r]
            [ui-components.layouts.main :refer [Main]]
            [ui-components.layouts.overlays :refer [overlays]]
            [ui-components.layouts.sidebar :refer [SidebarContainer]]
            [ui-components.layouts.sticky :refer [StickyContainer]]
            [ui-components.logo :refer [Logo]]
            [ui-components.menubar :refer [Menubar]]
            [ui-components.semantic :refer [Modal]]
            [shared.protocols.loggable :as log]))

(def ui-state (r/atom {:sidebar :hidden}))

(defn close-sidebar []
  (swap! ui-state assoc :sidebar :invisible))

(defn open-sidebar []
  (swap! ui-state assoc :sidebar :visible))

(defn BaseLayout [{:keys [logo site-title flags user app-mode notification handlers] :as appstate} main]
  (let [overlay (app-mode (overlays appstate))
        sidebar-visible? (= (:sidebar @ui-state) :visible)]
     [StickyContainer
      [Menubar {:logo [Logo site-title (:go-home handlers)]
                :user user
                :sign-in (:sign-in handlers)
                :sign-up (:sign-up handlers)
                :sign-out (:sign-out handlers)}]
      [SidebarContainer
       [:div]
       [:div overlay]
       [Main {:notification notification} main]]]))
