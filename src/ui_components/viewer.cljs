(ns ui-components.viewer
  (:require [ui-components.content :refer [Content]]
            [ui-components.content-meta :refer [ContentMeta]]
            [ui-components.semantic :refer [Grid GridColumn GridRow Loader]]))

(defn Viewer [{:keys [resource checkpoint]}]
  [Grid {:padded true}
   [GridRow {:reversed :mobile}
    [GridColumn {:mobile 16
                 :tablet 16
                 :computer 11
                 :widescreen 8}
     (if resource
       [Content resource]
       [Loader {:active true
                :size :massive
                :inline :centered} "Loading..."])]
    [GridColumn {:mobile 16
                 :tablet 16
                 :computer 5
                 :widescreen 4}
     [ContentMeta (merge checkpoint resource)]]]])
