(ns ui-components.menubar
  (:require [ui-components.actions-panel :refer [ActionsPanel]]
            [ui-components.semantic :refer [Button JSItem Icon Item ItemHeader Menu]]
            [shared.protocols.loggable :as log]))

(defn Menubar [{:keys [sign-in sign-up sign-out user logo]}]
  [Menu
   logo
   [ActionsPanel {:user-name (:user-name user)
                  :status (:status user)
                  :sign-up sign-up
                  :sign-in sign-in
                  :sign-out sign-out}]])
