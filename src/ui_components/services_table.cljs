(ns ui-components.services-table
  (:require [ui-components.semantic :refer [Table TableBody TableCell TableHeader
                                        TableHeaderCell TableRow]]))

(defn ServiceRow [[title info]]
  ^{:key title} [TableRow
                 [TableCell title]
                 (if (string? info)
                   ^{:key info} [TableCell info]
                   (map (fn [val] ^{:key val}[TableCell val])(vals info)))])


(defn ServicesTable [index [service-name service-data]]
  (let [orientation (if (= index 0) :top true)]
    ^{:key service-name}[Table {:celled true
                                :compact true
                                :attached orientation}
                         [TableHeader
                          [TableRow {:textAlign :center}
                           [TableHeaderCell {:colSpan 3} service-name]]]
                         [TableBody (map ServiceRow service-data)]]))
