(ns ui-components.logo
  (:require [ui-components.semantic :refer [Item]]))

(defn Logo [site-title respond]
  [Item {:className "logo"
         :on-click #(respond [:go :home])}
    [:img {:src "/images/logo.svg" :alt "offcourse logo"}]])
