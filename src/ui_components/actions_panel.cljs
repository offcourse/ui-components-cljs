(ns ui-components.actions-panel
  (:require [cuerdas.core :as str]
            [shared.protocols.loggable :as log]
            [ui-components.semantic :refer [Button Item Loader MenuMenu]]))

(defn LoggedInMenu [user-name sign-out]
  [MenuMenu {:position :right}
   [Item
    [Button {:basic true}
     [:a {:href "/courses/new"} "Create Course"]]]
   [Item
    [Button {:basic true} (str/capital user-name)]]
   [Item
    [Button {:basic true
             :on-click sign-out} "Sign Out"]]])

(defn LoadingMenu []
  [MenuMenu {:position :right}
   [Item [Loader {:active true
                  :size :mini
                  :inline true}]]])

(defn GuestMenu [sign-up sign-in]
  [MenuMenu {:position :right}
   [Item
    [Button {:basic true
             :on-click sign-up} "Sign Up"]]
   [Item
    [Button {:basic true
             :on-click #(sign-in nil)} "Sign In"]]])

(defn ActionsPanel [{:keys [status user-name sign-up sign-in sign-out]}]
  (case status
    :signed-in  [LoggedInMenu user-name sign-out]
    :signed-out [GuestMenu sign-up sign-in]
    [LoadingMenu]))
