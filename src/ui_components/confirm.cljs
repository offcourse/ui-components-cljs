(ns ui-components.confirm
  (:require [shared.protocols.loggable :as log]
            [ui-components.forms.sections.controls :refer [FormControls]]
            [ui-components.overlay :refer [Overlay]]
            [ui-components.semantic :refer [Form Modal ModalHeader ModalContent ModalActions Segment]]))

(defn Confirm [{:keys [on-cancel content title course on-confirm confirm-message]}]
  [Overlay {:title title}
   [:div content]
   [FormControls {:on-cancel on-cancel
                  :on-confirm #(on-confirm course)
                  :confirm-message confirm-message}]])
