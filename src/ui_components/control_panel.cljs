(ns ui-components.control-panel
  (:require [ui-components.semantic :refer [Button]]
            [shared.protocols.loggable :as log]))

(defn ControlPanel [{:keys [flags handlers]}]
  (let [{:keys [authenticatable? editable? navigatable? forkable?]} flags
        {:keys [edit-course go-to-fork fork-course sign-in]} handlers]
    (cond
      editable?        [Button {:onClick edit-course :fluid   true} "Edit"]
      navigatable?     [Button {:onClick go-to-fork :fluid   true} "Edit"]
      forkable?        [Button {:onClick fork-course :fluid   true} "Edit"]
      authenticatable? [Button {:onClick sign-in :fluid   true} "Edit"]
      :default         [Button {:disabled true :fluid true} "Edit"])))
