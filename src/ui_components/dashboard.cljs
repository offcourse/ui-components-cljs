(ns ui-components.dashboard
  (:require [ui-components.semantic :refer [Segment]]))

(defn Dashboard [content controls]
  [Segment {:floated :left
            :basic true}
   content
   controls])
