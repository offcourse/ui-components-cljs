(ns ui-components.course-card
  (:require [ui-components.checkpoint-list :refer [CheckpointList]]
            [ui-components.course-meta :refer [CourseMeta]]
            [ui-components.labels :refer [Labels]]
            [ui-components.semantic :refer [Card CardContent CardHeader]]
            [cuerdas.core :as str]
            [ui-components.social-menu :refer [SocialMenu]]
            [shared.protocols.loggable :as log]))

(defn CourseCard [{:keys [course trackable? toggle-status]}]
  (let [{:keys [urls description goal checkpoints tags external-url]} course]
    [Card
     [CardContent
      [CardHeader [:a {:href (:internal urls)} goal]]]
     [CardContent
      [CourseMeta course]]
     [CardContent
      [CheckpointList {:checkpoints checkpoints
                       :trackable? trackable?
                       :on-check toggle-status}]]
     (when description [CardContent
                        [:div [:strong "Course Description" ] [:br] description]])
     (when-not (empty? tags)
       [CardContent
        [Labels tags]])
     [CardContent
      [SocialMenu {:providers [:twitter :github :facebook]
                   :url (:external urls)}]]]))
