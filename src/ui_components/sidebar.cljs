(ns ui-components.sidebar
  (:require [ui-components.semantic :as s :refer [JSMenu Item Button JSItem Icon]]))

(defn Sidebar [{:keys [visible? toggle]} menu]
  [s/Sidebar {:animation :push
              :on-click toggle
              :size :huge
              :direction "right"
              :visible visible?}
   [Icon {:name (if visible? "caret right" "caret left")}]
   menu])
